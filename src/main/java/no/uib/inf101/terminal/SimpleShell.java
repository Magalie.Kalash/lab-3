package no.uib.inf101.terminal;

import java.io.File;

// UiB INF101 ShellLab - SimpleShell.java
//
// Dette er klassen vi skal forbedre i denne lab'en. Slik koden er
// allerede før du begynner på laben, støtter den tre kommandoer:
//
// - cd: Bytt til en annen mappe
// - ls: List opp filer i mappen
// - pwd: Vis sti til nåværende mappe
//
// Vi skal endre denne klassen slik at den
// - kan vises av Terminal.java
// - kan støtte ubegrenset mange kommandoer

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;


public class SimpleShell implements CommandLineInterface {

  //////////////////////////////////////////////////////////////////////
  /// Instance variables ///////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////

  /** The prompt to show before each command */
  private final String prompt = "$ ";
  /** The context variable contains cwd and home directories etc. */
  private final Context context = new Context();
  /** A list of historic commands and their outputs */
  private final List<String> history = new ArrayList<>();
  /** The command currently being typed */
  private String currentCommand = "";
  private final HashMap<String, Command> allCommands = new HashMap<>();

  /** Constructor for SimpleShell */
  public SimpleShell() {
    installCommand(new Pwd(context));
    installCommand(new Cd(context));
    installCommand(new Ls(context));
  }

  @Override
  public void keyPressed(char key) {
    if (key == '\n') {
      this.processCurrentCommandLine();
    } else if (key >= ' ' && key <= '~') {
      this.currentCommand += key;
    } else {
      // Some special key was pressed (e.g. shift, ctrl), we ignore it
    }
  }

  @Override
  public String getScreenContent() {
    String s = "";
    for (String line : this.history) {
      s += line;
    }
    s += this.prompt;
    s += this.currentCommand;
    return s;
  }

  public void installCommand(Command command) {
    this.allCommands.put(command.getName(), command);
  }

  /**
   * Process the current command line. This entails splitting it into
   * a command name and arguments; executing the command; and adding
   * the result to the history.
   */
  private void processCurrentCommandLine() {
    String result = "";
    if (this.currentCommand.length() > 0) {
      String[] args = this.currentCommand.split(" ");
      String commandName = args[0];
      String[] commandArgs = new String[args.length - 1];
      System.arraycopy(args, 1, commandArgs, 0, commandArgs.length);
      result = this.executeCommand(commandName, commandArgs);
      if (result.length() > 0 && result.charAt(result.length() - 1) != '\n') {
        result += '\n';
      }
    }
    this.history.add(this.prompt + this.currentCommand + "\n" + result);
    this.currentCommand = "";
  }

  private String executeCommand(String commandName, String[] args) {
    Command command = allCommands.get(commandName);
    if (Objects.nonNull(command)) {
      return command.run(args);
    } else {
      return "Command not found: "+ "\"" + commandName + "\"";
    }
  }


  
  public class Pwd implements Command {
    private Context contxt;
    public Pwd (Context context) {
      contxt = context;
    }

    @Override
    public String getName() {
      return "pwd";
    }
    @Override
    public String run(String[] args) {
      return contxt.getCwd().getAbsolutePath();
    }
  }

 
  public class Cd implements Command {
    Context contxt;
    public Cd(Context context) {
      contxt = context;
    }

    @Override
    public String getName() {
      return "cd";
    }
    @Override
    public String run(String[] args) {
      if (args.length == 0) {
        this.contxt.goToHome();
        return "";
      } else if (args.length > 1) {
        return "cd: too many arguments";
      }
      String path = args[0];
      if (this.contxt.goToPath(path)) {
        return "";
      } else {
        return "cd: no such file or directory: " + path;
      }
    }
  }

 
  public class Ls implements Command {
    Context contxt;
    public Ls(Context context) {
      contxt = context;
    }
    
    @Override
    public String getName() {
      return "ls";
    }

    @Override
    public String run(String[] args) {
      File cwd = this.contxt.getCwd();
      String s = "";
      for (File file : cwd.listFiles()) {
        s += file.getName();
        s += " ";
      }
      return s;
    }
    }

}