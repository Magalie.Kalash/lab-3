package no.uib.inf101.terminal;

public interface Command {
    String getName();
    String run(String[] args);
}
/**
Definere metoder i `Command`


Planen er at vi skal kunne kalle på  `run` på *Command* -objekter. Nærmere bestemt ønsker vi at følgende skal være mulig hvis *command* er en variabel som har typen *Command*:
```java 
String[] args = new String[] { "foo", "bar" };
String result = command.run(args);

//For at dette skal være lov, må grensesnittet *Command* definere en metodesignatur `String run(String[])`. La oss gjøre det nå. 

TODO: Legg til følgende metodesignatur i `Command`:




I tillegg trenger vi å vite hva navnet til kommandoen er.

TODO: Legg til følgende metodesignatur i `Command`:



**/